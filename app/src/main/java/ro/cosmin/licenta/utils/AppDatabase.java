package ro.cosmin.licenta.utils;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import ro.cosmin.licenta.interfaces.DatabaseDAO;
import ro.cosmin.licenta.models.Appoitment;
import ro.cosmin.licenta.models.Room;
import ro.cosmin.licenta.models.User;

@Database(entities = {User.class, Room.class, Appoitment.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract DatabaseDAO databaseDAO();
}
