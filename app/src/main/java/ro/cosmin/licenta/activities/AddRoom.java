package ro.cosmin.licenta.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ro.cosmin.licenta.R;
import ro.cosmin.licenta.models.Room;
import ro.cosmin.licenta.singleton.Database;

public class AddRoom extends BaseActivity {

    private EditText roomName;
    private EditText roomDescription;
    private Button addRoom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_room);

        init();
        setupActions();
    }

    private void setupActions() {
        addRoom.setOnClickListener(view -> {

            Room room = new Room();
            room.setName(roomName.getText().toString());
            room.setDescription(roomDescription.getText().toString());

            Observable.fromCallable(() -> Database.getInstance(getApplicationContext())
                    .getDatabase()
                    .databaseDAO()
                    .insertRoom(room)).subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(result -> {
                        AddRoom.this.finish();
                    });
        });
    }

    private void init() {
        roomName = findViewById(R.id.room_name);
        roomDescription = findViewById(R.id.room_description);
        addRoom = findViewById(R.id.add_room);
    }
}
