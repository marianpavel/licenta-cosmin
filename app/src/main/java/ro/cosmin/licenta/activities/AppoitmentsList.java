package ro.cosmin.licenta.activities;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ro.cosmin.licenta.R;
import ro.cosmin.licenta.adapters.AppoitmentAdapter;
import ro.cosmin.licenta.singleton.Database;

public class AppoitmentsList extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appoitments_list);

        mRecyclerView = findViewById(R.id.recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)


        fetchAppoitments();
    }

    @SuppressLint("CheckResult")
    private void fetchAppoitments() {
        Observable.fromCallable(() ->
                Database.getInstance(getApplicationContext())
                .getDatabase()
                .databaseDAO()
                .getAppoitments())
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(result -> {
                            if (result != null && result.size() > 0) {
                                mAdapter = new AppoitmentAdapter(result);
                                mRecyclerView.setAdapter(mAdapter);
                            }
                        });
    }
}
