package ro.cosmin.licenta.activities;

import android.annotation.SuppressLint;
import android.os.Debug;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ro.cosmin.licenta.R;
import ro.cosmin.licenta.models.Appoitment;
import ro.cosmin.licenta.models.Room;
import ro.cosmin.licenta.models.User;
import ro.cosmin.licenta.singleton.Database;
import ro.cosmin.licenta.singleton.LoginSession;

public class AddAppoitment extends BaseActivity {

    private Spinner roomPicker;
    private Button addAppoitment;
    private Room selectedRoom = null;
    private HashMap<String, Room> roomHashMap = new HashMap<>();
    private Calendar calendar;
    private String date = "";
    private List<Appoitment> appoitmentList;
    private List<Timepoint> disabledHours;
    private int year;
    private int month;
    private int day;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_appoitment);

        init();
        populateSpinner();
        getAppoitments();
    }

    @SuppressLint("CheckResult")
    private void populateSpinner() {
        Observable.fromCallable(() -> Database.getInstance(getApplicationContext())
                .getDatabase()
                .databaseDAO()
                .getRooms())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(rooms -> {

                    if (rooms != null && rooms.size() > 0) {
                        String[] items = new String[rooms.size()];

                        for (Room room:rooms) {
                            items[rooms.indexOf(room)] = room.getName();
                            roomHashMap.put(room.getName(), room);
                        }

                        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
                        roomPicker.setAdapter(adapter);
                        roomPicker.setSelection(-1, false);
                        setupActions();
                    }
                });
    }

    @SuppressLint("CheckResult")
    private void setupActions() {

        roomPicker.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedRoom = roomHashMap.get(adapterView.getItemAtPosition(i).toString());

                DatePickerDialog dialog = DatePickerDialog.newInstance((view1, year, monthOfYear, dayOfMonth) -> {

                    AddAppoitment.this.year = year;
                    AddAppoitment.this.month = dayOfMonth;
                    AddAppoitment.this.day = dayOfMonth;

                    calendar.set(Calendar.YEAR, year);
                    calendar.set(Calendar.MONTH, monthOfYear);
                    calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                    TimePickerDialog timePickerDialog = TimePickerDialog.newInstance((view2, hourOfDay, minute, second) -> {

                        calendar.set(Calendar.YEAR, AddAppoitment.this.year);
                        calendar.set(Calendar.MONTH, AddAppoitment.this.month);
                        calendar.set(Calendar.DAY_OF_MONTH, AddAppoitment.this.day);
                        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        calendar.set(Calendar.MINUTE, 0);

                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.GERMANY);
                        simpleDateFormat.setCalendar(calendar);

                        date = simpleDateFormat.format(calendar.getTime());

                    }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);

                    timePickerDialog.enableMinutes(false);
                    timePickerDialog.setMinTime(new Timepoint(8, 0, 0));
                    timePickerDialog.setMaxTime(new Timepoint(20, 0, 0));

                    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.GERMANY);
                    disabledHours = new ArrayList<>();
                    for (Appoitment appoitment : appoitmentList) {
                        Date date = format.parse(appoitment.getDatetime(), new ParsePosition(0));
                        calendar.setTime(date);

                        if (calendar.get(Calendar.DAY_OF_MONTH) == AddAppoitment.this.day) {
                            disabledHours.add(new Timepoint(calendar.get(Calendar.HOUR_OF_DAY), 0, 0));
                        }
                    }

                    if (disabledHours.size() > 0) {
                        timePickerDialog.setDisabledTimes(disabledHours.toArray(new Timepoint[disabledHours.size()]));
                    }

                    timePickerDialog.show(getFragmentManager(), "TimePicker");

                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                dialog.setMinDate(Calendar.getInstance());
                dialog.show(getFragmentManager(), "DatePicker");
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        addAppoitment.setOnClickListener(view -> {

            if (selectedRoom != null && date.length() > 0) {

                User user = LoginSession.getInstance().getSessionUser();
                if (user.getBuget() >= 10) {
                    Appoitment appoitment = new Appoitment();
                    appoitment.setRoom(selectedRoom.getName());
                    appoitment.setDatetime(date);
                    appoitment.setRoomDescription(selectedRoom.getDescription());
                    appoitment.setWho(LoginSession.getInstance().getSessionUser().getName());

                    Observable.fromCallable(() ->
                            Database.getInstance(getApplicationContext())
                                    .getDatabase()
                                    .databaseDAO()
                                    .insertAppoitment(appoitment))
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(result -> AddAppoitment.this.finish());

                    user.setBuget(user.getBuget() - 10);
                    Observable.fromCallable(() ->
                    Database.getInstance(getApplicationContext())
                    .getDatabase()
                    .databaseDAO()
                    .insertUser(user))
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(restult -> Log.i("TEST", "TEST"));
                } else {
                    Toast.makeText(getApplicationContext(), "Bugetul dumneavoastra este prea mic", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void init() {
        roomPicker = findViewById(R.id.room_picker);
        addAppoitment = findViewById(R.id.add_appoitment);
        calendar = Calendar.getInstance();
    }

    @SuppressLint("CheckResult")
    private void getAppoitments() {
        Observable.fromCallable(() -> Database.getInstance(getApplicationContext())
                .getDatabase()
                .databaseDAO()
                .getAppoitments())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> appoitmentList = result);
    }
}

