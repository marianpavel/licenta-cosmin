package ro.cosmin.licenta.activities;

import android.annotation.SuppressLint;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.evrencoskun.tableview.TableView;
import com.evrencoskun.tableview.filter.Filter;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ro.cosmin.licenta.R;
import ro.cosmin.licenta.adapters.TableViewAdapter;
import ro.cosmin.licenta.models.Cell;
import ro.cosmin.licenta.models.ColumnHeader;
import ro.cosmin.licenta.models.RowHeader;
import ro.cosmin.licenta.models.User;
import ro.cosmin.licenta.singleton.Database;
import ro.cosmin.licenta.viewmodels.TableViewModel;

import static java.security.AccessController.getContext;

public class Utilizatori extends AppCompatActivity {

    private EditText search;
    private List<RowHeader> mRowHeaderList = new ArrayList<>();
    private List<ColumnHeader> mColumnHeaderList = new ArrayList<>();
    private List<List<Cell>> mCellList = new ArrayList<>();
    private TableViewAdapter adapter;
    private Filter tableFilter;

    private TextWatcher mSearchTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            tableFilter.set(String.valueOf(s));
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_utilizatori);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        search = findViewById(R.id.query_string);
        search.addTextChangedListener(mSearchTextWatcher);

        TableView tableView = findViewById(R.id.content_container);
        tableFilter = new Filter(tableView);

        adapter = new TableViewAdapter(getApplicationContext());

        // Set this adapter to the our TableView
        tableView.setAdapter(adapter);

        fetchData();

    }

    @SuppressLint("CheckResult")
    private void fetchData() {
        Observable.fromCallable(Database.getInstance(getApplicationContext())
                .getDatabase()
                .databaseDAO()::getUsers)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {

                    mColumnHeaderList.add(new ColumnHeader("0", "Nume"));
                    mColumnHeaderList.add(new ColumnHeader("1", "Email"));
                    mColumnHeaderList.add(new ColumnHeader("2", "Localitate"));
                    mColumnHeaderList.add(new ColumnHeader("3", "Numar Telefon"));
                    mColumnHeaderList.add(new ColumnHeader("4", "Parola Contului"));

                    for (User user : result) {
                        List<Cell> cells = new ArrayList<>();
                        cells.add(new Cell(String.valueOf(user.getId()), user.getName()));
                        cells.add(new Cell(String.valueOf(user.getId()), user.getEmail()));
                        cells.add(new Cell(String.valueOf(user.getId()), user.getLocalitate()));
                        cells.add(new Cell(String.valueOf(user.getId()), user.getTelefon()));
                        cells.add(new Cell(String.valueOf(user.getId()), user.getPassword()));
                        mCellList.add(cells);
                        mRowHeaderList.add(new RowHeader(String.valueOf(result.indexOf(user) + 1),
                                String.valueOf(result.indexOf(user) + 1)));
                    }

                    // Let's set datas of the TableView on the Adapter
                    adapter.setAllItems(mColumnHeaderList, mRowHeaderList, mCellList);
                });
    }
}
