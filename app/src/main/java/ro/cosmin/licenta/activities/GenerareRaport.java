package ro.cosmin.licenta.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.pdfjet.Cell;
import com.pdfjet.Color;
import com.pdfjet.CoreFont;
import com.pdfjet.Font;
import com.pdfjet.Letter;
import com.pdfjet.PDF;
import com.pdfjet.Page;
import com.pdfjet.Point;
import com.pdfjet.Table;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ro.cosmin.licenta.R;
import ro.cosmin.licenta.models.User;
import ro.cosmin.licenta.singleton.Database;

public class GenerareRaport extends AppCompatActivity {

    private Page page;
    private ProgressBar loading;
    private Button vizualizare;
    private Button trimitere;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generare_raport);

        loading = findViewById(R.id.loading);
        vizualizare = findViewById(R.id.vizualizare);
        trimitere = findViewById(R.id.trimitere);

        vizualizare.setEnabled(false);
        trimitere.setEnabled(false);

        vizualizare.setOnClickListener(view -> {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());

            File file = new File(getExternalFilesDir("pdf").getAbsolutePath() + "/Raport.pdf");
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(file), "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        });

        trimitere.setOnClickListener(view -> {
            Intent intentShareFile = new Intent(Intent.ACTION_SEND);
            File fileWithinMyDir = new File(getExternalFilesDir("pdf").getAbsolutePath() + "/Raport.pdf");

            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());

            if(fileWithinMyDir.exists()) {
                intentShareFile.setType("application/pdf");
                intentShareFile.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://"+fileWithinMyDir.getPath()));

                intentShareFile.putExtra(Intent.EXTRA_SUBJECT,
                        "Sharing File...");
                intentShareFile.putExtra(Intent.EXTRA_TEXT, "Sharing File...");

                startActivity(Intent.createChooser(intentShareFile, "Share File"));
            }
        });

        try {
            generatePDF();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @SuppressLint("CheckResult")
    private void generatePDF() throws Exception {
        FileOutputStream fos = new FileOutputStream(getExternalFilesDir("pdf").getAbsolutePath() + "/Raport.pdf");

        PDF pdf = new PDF(fos);
        page = new Page(pdf, Letter.PORTRAIT);

        Font f1 = new Font(pdf, CoreFont.HELVETICA_BOLD);
        f1.setSize(7.0f);

        Font f2 = new Font(pdf, CoreFont.HELVETICA);
        f2.setSize(7.0f);

        Font f3 = new Font(pdf, CoreFont.HELVETICA_BOLD_OBLIQUE);
        f3.setSize(7.0f);

        Table table = new Table();
        List<List<Cell>> tableData = new ArrayList<>();


        Observable.fromCallable(() -> Database.getInstance(getApplicationContext())
                .getDatabase()
                .databaseDAO()
                .getUsers())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {

                    List<Cell> header = new ArrayList<>();
                    header.add(new Cell(f2, "Nume"));
                    header.add(new Cell(f2, "Email"));
                    header.add(new Cell(f2, "Localitate"));
                    header.add(new Cell(f2, "Telefon"));
                    header.add(new Cell(f2, "Parola"));
                    tableData.add(header);

                    for (User user : result) {
                        List<Cell> cells = new ArrayList<>();
                        cells.add(new Cell(f1, user.getName()));
                        cells.add(new Cell(f1, user.getEmail()));
                        cells.add(new Cell(f1, user.getLocalitate()));
                        cells.add(new Cell(f1, user.getTelefon()));
                        cells.add(new Cell(f1, user.getPassword()));
                        tableData.add(cells);
                    }

                    table.setData(tableData, Table.DATA_HAS_1_HEADER_ROWS);
                    table.setPosition(70.0f, 30.0f);
                    table.autoAdjustColumnWidths();
                    table.rightAlignNumbers();
                    int numOfPages = table.getNumberOfPages(page);
                    while (true) {
                        Point point = table.drawOn(page);
                        // TO DO: Draw "Page 1 of N" here
                        if (!table.hasMoreData()) {
                            // Allow the table to be drawn again later:
                            table.resetRenderedPagesCount();
                            break;
                        }
                        page = new Page(pdf, Letter.PORTRAIT);
                    }

                    pdf.flush();
                    fos.close();
                    loading.setVisibility(View.GONE);
                    vizualizare.setEnabled(true);
                    trimitere.setEnabled(true);
                });
    }
}
