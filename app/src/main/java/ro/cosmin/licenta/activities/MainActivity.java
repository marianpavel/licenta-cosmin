package ro.cosmin.licenta.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

import ro.cosmin.licenta.R;
import ro.cosmin.licenta.models.User;
import ro.cosmin.licenta.singleton.LoginSession;

public class MainActivity extends BaseActivity {

    private Button addUser;
    private Button login;
    private Button viewAll;
    private Button addAppoitment;
    private Button addRoom;
    private Button generareRaport;
    private Button utilizatori;
    private TextView proverbHolder;
    private String[] proverbe;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        init();
        setupActions();
    }

    private void setupActions() {
        addUser.setOnClickListener(v -> {
            startActivity(new Intent(this, AddUser.class));
        });

        login.setOnClickListener(v -> {
            if (LoginSession.getInstance().getSessionUser() != null) {
                LoginSession.getInstance().setSessionUser(null);
                finish();
                startActivity(getIntent());
            } else {
                startActivity(new Intent(this, Login.class));
            }
        });

        viewAll.setOnClickListener(v -> {
            startActivity(new Intent(this, AppoitmentsList.class));
        });

        addAppoitment.setOnClickListener(v -> {
            startActivity(new Intent(this, AddAppoitment.class));
        });

        addRoom.setOnClickListener(v -> {
            startActivity(new Intent(this, AddRoom.class));
        });

        generareRaport.setOnClickListener(v -> {
            startActivity(new Intent(this, GenerareRaport.class));
        });

        utilizatori.setOnClickListener(v -> {
            startActivity(new Intent(this, Utilizatori.class));
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (LoginSession.getInstance().getSessionUser() != null) {
            User user = LoginSession.getInstance().getSessionUser();

            login.setText(R.string.logout);
            addUser.setVisibility(View.GONE);

            if (user.getAdmin()) {
                addRoom.setVisibility(View.VISIBLE);
                generareRaport.setVisibility(View.VISIBLE);
                utilizatori.setVisibility(View.VISIBLE);
            }

            addAppoitment.setVisibility(View.VISIBLE);
        }


        proverbHolder.setText(proverbe[new Random().nextInt(proverbe.length)]);

    }

    private void init() {
        addUser = findViewById(R.id.add_user);
        login = findViewById(R.id.login);
        viewAll = findViewById(R.id.view_all);
        addAppoitment = findViewById(R.id.add_appoitment);
        addRoom = findViewById(R.id.add_room);
        proverbHolder = findViewById(R.id.proverb);
        proverbe = getResources().getStringArray(R.array.proverbe);
        generareRaport = findViewById(R.id.generare_raport);
        utilizatori = findViewById(R.id.utilizatori);
    }
}
