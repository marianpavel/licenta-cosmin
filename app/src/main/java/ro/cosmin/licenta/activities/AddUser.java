package ro.cosmin.licenta.activities;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ro.cosmin.licenta.R;
import ro.cosmin.licenta.models.User;
import ro.cosmin.licenta.singleton.Database;

public class AddUser extends BaseActivity {

    private Button add;
    private EditText user;
    private EditText password;
    private EditText email;
    private EditText localitate;
    private EditText telefon;
    private EditText buget;
    private CheckBox admin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);

        init();
        setupActions();
    }

    private void setupActions() {
        add.setOnClickListener(v -> {
            addUser(user.getText().toString(), password.getText().toString(), admin.isChecked());
        });
    }

    @SuppressLint("CheckResult")
    private void addUser(String user, String password, Boolean isAdmin) {

        /**
         * Creeam un user model
         */
        User userModel = new User();
        userModel.setName(user);
        userModel.setPassword(password);
        userModel.setAdmin(isAdmin);
        userModel.setTelefon(telefon.getText().toString());
        userModel.setLocalitate(localitate.getText().toString());
        userModel.setEmail(email.getText().toString());
        userModel.setBuget(Integer.parseInt(buget.getText().toString()));

        /**
         * Inseram userul in baza de date
         */
        Observable.fromCallable(() -> Database.getInstance(getApplicationContext())
                .getDatabase().databaseDAO()
                .insertUser(userModel)).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((result) -> AddUser.this.finish());
    }

    private void init() {
        add = findViewById(R.id.add);
        user = findViewById(R.id.user);
        password = findViewById(R.id.password);
        admin = findViewById(R.id.admin);
        email = findViewById(R.id.email);
        localitate = findViewById(R.id.localitate);
        telefon = findViewById(R.id.telefon);
        buget = findViewById(R.id.buget);
    }
}
