package ro.cosmin.licenta.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ro.cosmin.licenta.R;
import ro.cosmin.licenta.interfaces.LoginListener;
import ro.cosmin.licenta.models.User;
import ro.cosmin.licenta.singleton.Database;
import ro.cosmin.licenta.singleton.LoginSession;

public class Login extends BaseActivity {

    private EditText user;
    private EditText password;
    private Button login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();
        setupActions();
    }

    private void setupActions() {
        login.setOnClickListener(v -> {
            getUsers(user.getText().toString(), password.getText().toString(), users -> {
                boolean found = false;

                /**
                 * Iteram prin fiecare user existent
                 * si
                 * verificam daca campurile completate coincid cu un user existent
                 */
                for (User user:users) {
                    if (user.getName().equals(Login.this.user.getText().toString()) &&
                            user.getPassword().equals(password.getText().toString())) {
                        found = true;
                        LoginSession.getInstance().setSessionUser(user);
                        break;
                    }
                }

                /**
                 * Daca am gasit ne intoarcem la ecranul dinainte
                 */
                if (found) {
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Username or password incorrect", Toast.LENGTH_SHORT).show();
                }
            });
        });
    }

    private void init() {
        user = findViewById(R.id.user);
        password = findViewById(R.id.password);
        login = findViewById(R.id.login);
    }

    @SuppressLint("CheckResult")
    void getUsers(String user, String password, LoginListener listener) {

        Observable.fromCallable(() -> Database.getInstance(getApplicationContext())
        .getDatabase()
        .databaseDAO()
        .getUsers()).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(listener::onResultReady);
    }
}
