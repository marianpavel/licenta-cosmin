package ro.cosmin.licenta.singleton;

import ro.cosmin.licenta.models.User;

public class LoginSession {
    private static final LoginSession ourInstance = new LoginSession();
    private User user = null;

    public static LoginSession getInstance() {
        return ourInstance;
    }

    private LoginSession() {
    }

    public void setSessionUser(User user) {
        this.user = user;
    }

    public User getSessionUser() {
        return user;
    }
}
