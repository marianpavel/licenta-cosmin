package ro.cosmin.licenta.singleton;

import android.arch.persistence.room.Room;
import android.content.Context;

import ro.cosmin.licenta.utils.AppDatabase;

public class Database {

    private static Database database = null;
    private static AppDatabase db;

    public static Database getInstance(Context context) {
        if (database == null) {
            database = new Database();
            db = Room.databaseBuilder(context,
                    AppDatabase.class, "database").build();
        }

        return database;
    }

    public AppDatabase getDatabase() {
        return db;
    }
}
