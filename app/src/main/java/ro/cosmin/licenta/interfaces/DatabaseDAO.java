package ro.cosmin.licenta.interfaces;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import ro.cosmin.licenta.models.Appoitment;
import ro.cosmin.licenta.models.Room;
import ro.cosmin.licenta.models.User;

@Dao
public interface DatabaseDAO {

    @Query("SELECT * FROM user")
    List<User> getUsers();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertUser(User user);

    @Insert
    long insertRoom(Room room);

    @Query("SELECT * FROM room")
    List<Room> getRooms();

    @Insert
    long insertAppoitment(Appoitment appoitment);

    @Query("SELECT * FROM appoitment")
    List<Appoitment> getAppoitments();
}
