package ro.cosmin.licenta.interfaces;

import java.util.List;

import ro.cosmin.licenta.models.User;

public interface LoginListener {

    void onResultReady(List<User> users);
}
