package ro.cosmin.licenta.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ro.cosmin.licenta.R;
import ro.cosmin.licenta.models.Appoitment;

public class AppoitmentAdapter extends RecyclerView.Adapter<AppoitmentAdapter.ViewHolder> {
    private List<Appoitment> appoitments;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private TextView text;
        private TextView description;
        private TextView data;
        private TextView ora;
        private TextView who;
        ViewHolder(View v) {
            super(v);
            text = v.findViewById(R.id.text);
            description = v.findViewById(R.id.description);
            data = v.findViewById(R.id.data);
            ora = v.findViewById(R.id.ora);
            who = v.findViewById(R.id.who);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AppoitmentAdapter(List<Appoitment> list) {
        appoitments = list;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public AppoitmentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_appoitment_list, parent, false);

        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.text.setText(String.format("Sala: %s", appoitments.get(position).getRoom()));
        holder.description.setText(appoitments.get(position).getRoomDescription());
        holder.who.setText(String.format("Sedinta adaugata de: %s", appoitments.get(position).getWho()));
        String dateTime = appoitments.get(position).getDatetime();
        holder.data.setText(String.format("Data: %s", dateTime.substring(0,
                dateTime.indexOf(" "))));
        holder.ora.setText(String.format("Ora: %s", dateTime.substring(dateTime.indexOf(" "))));

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return appoitments.size();
    }
}
