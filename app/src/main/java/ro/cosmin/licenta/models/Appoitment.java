package ro.cosmin.licenta.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Appoitment {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "room")
    private String room;

    @ColumnInfo(name = "datetime")
    private String datetime;

    @ColumnInfo(name = "roomDescription")
    private String roomDescription;

    @ColumnInfo(name = "who")
    private String who;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getRoomDescription() {
        return roomDescription;
    }

    public void setRoomDescription(String roomDescription) {
        this.roomDescription = roomDescription;
    }

    public String getWho() {
        return who;
    }

    public void setWho(String who) {
        this.who = who;
    }
}
